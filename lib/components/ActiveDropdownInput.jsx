
import React from 'react';
import {Input} from 'react-bootstrap';
import FormGroup from 'react-bootstrap/lib/FormGroup'
import classnames from 'classnames';
import _ from 'underscore';
import DropdownInput from 'framework/Widgets/DropdownInput';

export default  class ActiveDropdownInput extends React.Component {

    constructor(props) {
        super();
        var {model, attr, ...otherProps} = props;
        this.model = model;
        this.attr = attr;
        this.textAttr = otherProps.textAttr || 'name';
        this.otherProps = otherProps;
        this.isSelected = false;
        this.state = {
            help:null,
            bsStyle:null,
            label:model.getLabel(attr)
        };
    }

    componentDidMount(){
        this.model.on('validated',this.checkValid,this)
    }
    componentWillUnmount(){
        this.model.off(null,null,this)
    }

    checkValid(){
        this.validate(this.model.get(this.attr));
    }
    onSelectItem = (model,e)=>{
        this.validate(model.get('id'));
        this.isSelected = true;
    };
    onChangeInput = (e)=>{
        this.isSelected = false;
        this.model.unset(this.attr);
    };

    onRootClose = (e)=>{
        if(!this.isSelected){
           // this.validate(null);
        }
    };

    validate(value){
        this.model.set(this.attr,value);
        let newState = {},
            msg;
        if( !(msg = this.model.preValidate(this.attr,value))){
            newState.help = null;
            newState.bsStyle = 'success';

        }else{
            newState.help = msg;
            newState.bsStyle = 'error';
        }
        this.setState(newState);
    }

    renderHelp() {
        return this.state.help ? React.createElement(
            'span',
            { className: 'help-block', key: 'help' },
            this.state.help
        ) : null;
    };


    renderLabel(children) {
        var classes = {
            'control-label': true
        };
        let classNames = classnames(classes,this.props.labelClassName);
        let label = this.props.label || this.state.label;
        return label ? React.createElement(
            'label',
            { htmlFor: this.props.id, className: classNames , key: 'label' },
            children,
            label
        ) : children;
    };

    renderWrapper(children) {
        return this.props.wrapperClassName ? React.createElement(
            'div',
            { className: this.props.wrapperClassName, key: 'wrapper' },
            children
        ) : children;
    };

    renderInputGroup(children) {
        var addonBefore = this.props.addonBefore ? React.createElement(
            'span',
            { className: 'input-group-addon', key: 'addonBefore' },
            this.props.addonBefore
        ) : null;

        var addonAfter = this.props.addonAfter ? React.createElement(
            'span',
            { className: 'input-group-addon', key: 'addonAfter' },
            this.props.addonAfter
        ) : null;

        var buttonBefore = this.props.buttonBefore ? React.createElement(
            'span',
            { className: 'input-group-btn' },
            this.props.buttonBefore
        ) : null;

        var buttonAfter = this.props.buttonAfter ? React.createElement(
            'span',
            { className: 'input-group-btn' },
            this.props.buttonAfter
        ) : null;

        var inputGroupClassName = undefined;
        switch (this.props.bsSize) {
            case 'small':
                inputGroupClassName = 'input-group-sm';break;
            case 'large':
                inputGroupClassName = 'input-group-lg';break;
            default:
        }

        return addonBefore || addonAfter || buttonBefore || buttonAfter ? React.createElement(
            'div',
            { className: classnames(inputGroupClassName, 'input-group'), key: 'input-group' },
            addonBefore,
            buttonBefore,
            children,
            addonAfter,
            buttonAfter
        ) : children;
    };


    renderInput(){
        let defaultValue ='';
        if(this.model.get(this.attr) !== undefined && this.model.get(this.attr)[this.textAttr] !== undefined){
           defaultValue = this.model.get(this.attr)[this.textAttr];
        }else{
           console.warn('Undefined model attribute: ' + this.attr + '.' + this.textAttr);
        }
        return (<DropdownInput {...this.otherProps} key={1} defaultValue={defaultValue} onSelectItem={this.onSelectItem} onChangeInput={this.onChangeInput} onRootClose={this.onRootClose}/>)
    }

    renderFormGroup(children) {
        return React.createElement(
            FormGroup,
            this.state,
            children
        );
    };

    renderChildren() {
        return [this.renderLabel(), this.renderWrapper([this.renderInputGroup(this.renderInput()), this.renderHelp()])];
    };
    render() {
        var children = this.renderChildren();
        return this.renderFormGroup(children);
    };
}

