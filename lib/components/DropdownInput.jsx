
import React from 'react';
import {DropdownMenu, MenuItem} from 'react-bootstrap';
import RootCloseWrapper from 'react-bootstrap/node_modules/react-overlays/lib/RootCloseWrapper';
import classnames from 'classnames';
import _ from 'underscore';

export default  class DropdownInput extends React.Component {

    constructor(props) {
        var {collection ,filterAttr,textAttr,onSelectItem,filterCollection,debounceTime,onChangeInput,onRootClose ,  ...otherProps} = props;
        super(otherProps);
        this.collection = collection;
        this.filterAttr = filterAttr || 'name';
        this.textAttr = textAttr || 'name';
        this.proxyOnSelectItem = onSelectItem || null;
        this.proxyOnChangeInput = onChangeInput || null;
        this.proxyOnRootClose =  onRootClose || null;
        this.debounceTime = debounceTime || 400;
        if(typeof filterCollection === 'function'){
            this.filterCollection = filterCollection
        }
        this.state = {
            open: false,
            value:props.value
        };

        this.updateListData = _.debounce((value,silent = false)=>{
            this.filterCollection(this.collection,value).then(()=>{
                this.forceUpdate(()=>{
                    if(this.collection.length && !silent){
                        this.showDropdown()
                    }else{
                        this.closeDropdown()
                    }
                })
            })
        },  this.debounceTime);
    }

    componentWillReceiveProps(nextProps){
        if(nextProps.value !== undefined){
            this.setState({value:nextProps.value});
        }
    }

    filterCollection(collection,value){
        return collection.Find().where({[this.filterAttr]:{contains:value}}).All();
    }

    inputOnChange = (e)=> {
        e.preventDefault();
        e.stopPropagation();
        e.nativeEvent.stopImmediatePropagation();
        if(this.proxyOnChangeInput !== null){
            this.proxyOnChangeInput(e);
        }
        this.updateListData(e.target.value);
    };

    inputOnClick = (e)=>{
        e.preventDefault();
        e.stopPropagation();
        e.nativeEvent.stopImmediatePropagation();
        this.filterCollection(this.collection,e.target.value).then(()=>{
            if(this.collection.length){
                this.showDropdown()
            }
        })
    };

    itemOnClickCall(model){
        return (e)=>{
            if(this.proxyOnSelectItem !== null){
                this.proxyOnSelectItem(model,e);
            }
            this.refs.input.value = model.get(this.textAttr);
            this.updateListData(model.get(this.textAttr),true);
            this.closeDropdown();
        }
    }

    onRootClose = (e)=>{
        console.log('onRootClose')
        this.closeDropdown();
        if(this.proxyOnRootClose !== null){
            this.proxyOnRootClose(e)
        }
    };

    showDropdown(){
        this.setState({
            open:true
        })
    }
    closeDropdown(){
        this.setState({
            open:false
        })
    }

    renderInputGroup(children) {
        var addonBefore = this.props.addonBefore ? React.createElement(
            'span',
            { className: 'input-group-addon', key: 'addonBefore' },
            this.props.addonBefore
        ) : null;

        var addonAfter = this.props.addonAfter ? React.createElement(
            'span',
            { className: 'input-group-addon', key: 'addonAfter' },
            this.props.addonAfter
        ) : null;

        var buttonBefore = this.props.buttonBefore ? React.createElement(
            'span',
            { className: 'input-group-btn' },
            this.props.buttonBefore
        ) : null;

        var buttonAfter = this.props.buttonAfter ? React.createElement(
            'span',
            { className: 'input-group-btn' },
            this.props.buttonAfter
        ) : null;

        var inputGroupClassName = undefined;
        switch (this.props.bsSize) {
            case 'small':
                inputGroupClassName = 'input-group-sm';break;
            case 'large':
                inputGroupClassName = 'input-group-lg';break;
            default:
        }

        return addonBefore || addonAfter || buttonBefore || buttonAfter ? React.createElement(
            'div',
            { className: classnames(inputGroupClassName, 'input-group'), key: 'input-group' },
            addonBefore,
            buttonBefore,
            children,
            addonAfter,
            buttonAfter
        ) : children;
    };

    renderDropdown(){
        let className = classnames( this.props.className || 'dropdown', {
            'open': this.state.open
        });
        return (
            <div className={className}>
                <input className="form-control" onClick={this.inputOnClick} defaultValue={this.props.defaultValue} value={this.state.value} style={{float:'none'}} onChange={this.inputOnChange} ref={'input'} autoComplete="off" autoCorrect="off" autoCapitalize="off" spellCheck="false" />
                <RootCloseWrapper noWrap onRootClose={this.onRootClose}>
                    <ul className="dropdown-menu" style={{width:'100%'}}>
                        {this.collection.map((model)=>{
                            return  <li key={model.get('id')} onClick={this.itemOnClickCall(model)}><a style={{cursor:'pointer'}}>{model.get(this.textAttr)}</a></li>
                        })}

                    </ul>
                </RootCloseWrapper>
            </div>
        );
    }

    render() {
       return this.renderInputGroup(this.renderDropdown())
    }
}

