
import React from 'react';
var _ = require('underscore');


export default class ShortText extends React.Component{

    constructor(props){
        super(props);
    }

    getStyle(){
        return _.defaults(this.props.style || {},{
            'maxWidth':'400px',
            whiteSpace: 'nowrap',
            'textOverflow': 'ellipsis',
            overflow: 'hidden',
            display:'block'
        })
    }

    componentWillReceiveProps(props){

    }


    render() {
        return <div style={this.getStyle()}>{this.props.children}</div>
    }
}
