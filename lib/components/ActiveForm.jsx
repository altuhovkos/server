
import React from 'react';


export default  React.createClass({


    render() {
        return (
            <div className="container">
                <nav className="navbar navbar-inverse">
                    <div className="container-fluid">
                        <div className="navbar-header">
                            <a className="navbar-brand" href="#">Isomorphic Application</a>
                        </div>
                    </div>
                </nav>
                <div className="container-fluid well">
                    {this.props.children}
                </div>
            </div>

        );

    }
})
