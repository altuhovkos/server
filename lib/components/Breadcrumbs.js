
import React from 'react';


export default  React.createClass({



    click(url){
        return (e)=>{
            e.preventDefault();
            this.props.navigation.go(url)
        }
    },

    render() {
        return (
                    <ol className="breadcrumb">
                        {this.props.items.map((item,i)=>{
                            if(typeof item === 'object'){
                                return (<li key={i}><a href={item[1]} onClick={this.click(item[1])}>{item[0]}</a></li>)
                            }else{
                                return (<li key={i} className="active">{item}</li>)
                            }
                        })}
                    </ol>
        );

    }
})
