
import React from 'react';
import { Input } from 'react-bootstrap';


export default  class ActiveInput extends React.Component{


    constructor(props){
        var {model, attr ,  ...otherProps} = props;
        super(otherProps);
        this.model = model;
        this.attr = attr;
        this.state = {
            help:null,
            bsStyle:null,
            label:model.getLabel(attr)
        };
    }


    componentDidMount(){
        this.model.on('validated',this.checkValid,this)
        this.model.on('change:'+this.attr,this.changed,this)
    }
    componentWillUnmount(){
        this.model.off(null,null,this)
    }

    changed(){
        this.forceUpdate();
    }

    checkValid(){
        this.validate(this.model.get(this.attr));
    }
    onChange = (e)=>{
        this.validate(e.target.value);
    };


    validate(value){
        this.model.set(this.attr,value);
        let newState = {},
            msg;
        if( !(msg = this.model.preValidate(this.attr,value))){
            newState.help = null;
            newState.bsStyle = 'success';

        }else{
            newState.help = msg;
            newState.bsStyle = 'error';
        }
        this.setState(newState);
    }

    renderTypeText(){
        return ( <Input  onChange={this.onChange}  value={this.model.get(this.attr)} ref={(c) => this._input = c} label={this.state.label}  help={this.state.help}  bsStyle={this.state.bsStyle} {...this.props}/>)
    }
    renderTypeSelect(){
        let {elements,...props} = this.props;
        return ( <Input onChange={this.onChange} value={this.model.get(this.attr)} ref={(c) => this._input = c} label={this.state.label}  help={this.state.help}  bsStyle={this.state.bsStyle} {...props}>
            {elements.map((value,i)=>{
                return  (<option key={i} value={value}>{value}</option>)
            })}
        </Input>)
    }
    renderTypeRadio(){
        this._radio = [];
        let {elements,type, ...props} = this.props;
        return (   <Input  {...props} help={this.state.help}  bsStyle={this.state.bsStyle}>
            {elements.map((el,i)=>{
                return <Input name={this.attr} key={i} checked={this.model.get(this.attr) == el.value} ref={(c) => this._radio.push(c)} onChange={this.onChange} type="radio"  groupClassName="col-sm-4" {...el}  />
            })}
        </Input>)
    }

    render() {
        if(this.props.type === 'text' || this.props.type === 'textarea'){
            return this.renderTypeText();
        }else if(this.props.type === 'select' ){
            return  this.renderTypeSelect();
        }else if(this.props.type === 'radio' ){
            return this.renderTypeRadio();
        }
    }
}
