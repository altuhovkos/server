
var Router =  require('./router/isomorphic');
var Controller =  require('./controller');

module.exports = {

    Controller:{
      path: 'controllers/'
    },
    Router:{
        instance:Router,
        baseController: Controller,
        routes:[],
        parseUrl:{
            urlParamsDelimiter : new RegExp('([^\/])+','ig'),
            urlParamKeyValueDelimiter: '='
        },
        createUrl:{
            urlParamsDelimiter : '/',
            urlParamKeyValueDelimiter: '='
        }
    }

};