
import React from 'react';
import Helmet from 'react-helmet';

export default  React.createClass({


    render() {
        return (
            <div className="container-fluid">
                <Helmet {...this.props.page.toJSON()} />
                {this.props.children}
            </div>

        );

    }
})
