import Backbone from 'backbone';
var _ = require('underscore');

    var server = function (collection) {
        var _criteria = {},
            _pagination = {};
        _pagination.count_items = false;
        _pagination.countIsChanged = true;

        this.getCriteria = function (clear) {
            //_criteria.where ={"roles.user":true};

            var res = {};
            for(var v in _criteria){
                /*if(v == 'sort'){
                    res.sort = _criteria.sort;
                    res.sort = _criteria.sort + ' '+ _criteria.order;
                    continue;
                }*/
                if(v == 'order'){
                    continue;
                }
                if(v == 'populate'){
                    res[v] = _criteria[v].join(', ');
                    continue;
                }
                if(_.isObject(_criteria[v])){
                    res[v] = JSON.stringify(_criteria[v])
                }else{
                    res[v] = _criteria[v];
                }
            }
            //console.log('criteria: ',_criteria)
           // console.log('res: ',res)
            // res.sort = 'email ASC username ASC';
            return res;
        };

        this.clearCriteria = function(){
            _criteria = {};
            return this;
        };

        this.reset = function(){
            this.clearCriteria();
            return collection;
        };

        this.sort = function (param) {
            if (_.isUndefined(param) || !param) {
                delete _criteria.sort;
                delete _criteria.order;
            } else if (_.isString(param)) {
                _criteria.sort = param;
                /*if (_.isUndefined(_criteria.sort) || (!_.isUndefined(_criteria.sort) && _criteria.sort !== param)) {
                    _criteria.sort = param;
                    this.order('ASC');
                } else {
                    this.order();
                }*/
            }
            return this;
        };

        this.order = function (str) {
            if (_.isUndefined(_criteria.sort)) {
                this.sort('id');
            } else {
                if (_.isUndefined(str)) {
                    _criteria.order = _criteria.order === "ASC" ? "DESC" : "ASC";
                } else if (_.isString(str) && (str = str.toUpperCase()) && (( str === 'ASC' || str === 'DESC'))) {
                    _criteria.order = str;
                }
            }
            return this;
        };

        this.with = function (arr) {
            _.isUndefined(_criteria.populate) && (_criteria.populate = []);
            _criteria.populate = _criteria.populate.concat(arr);
            return this;
        };

        this.where = function (obj,second) {

            function parseObject(obj,key,value){
                if(_.isEmpty(value)){
                    delete obj[key];
                }else{
                    (_.isUndefined(obj[key]) || !_.isObject(obj[key])) && (obj[key] = {});
                    _.extend(obj[key],value);
                    obj[key] = _.omit(obj[key], function(v, k, o) {
                        return v === '';
                    });
                    _.isEmpty(obj[key]) && (delete obj[key]);
                }
                return obj;
            }
            function parseOther(obj,key,value){
                if(value === ''){
                    delete obj[key];
                }else{
                    obj[key] = value;
                }
                return obj
            }
            function parseArray(obj,key,value){
                if(value.length > 0){
                    _criteria.where[key] = value;
                }else{
                    delete _criteria.where[key];
                }
                return _criteria.where;
            }
            if (_.isObject(obj)) {
                _.isUndefined(_criteria.where) && (_criteria.where = {});
                _.each(obj,function(value,key){
                    if(_.isArray(value)){
                        _criteria.where = parseArray(_criteria.where[key] || {},key,value);
                    }else if (_.isObject(value)) {
                        _criteria.where = parseObject(_criteria.where,key,value);
                    }else{
                        _criteria.where = parseOther(_criteria.where,key,value);
                    }
                });
                (!_.isUndefined(_criteria.where) && _.isEmpty(_criteria.where)) && (delete _criteria.where);
            }else if(!_.isUndefined(second)){
                _criteria = parseOther(_criteria,obj,second);
            }
            return this;
        };

        this.All = function (search, options) {
            options = _.extend({reset:true},options || {});
            !_.isUndefined(collection.withDefault) && collection.withDefault();
            !_.isUndefined(search) && search && this.where(search);
            //console.log('collection fetch, options:',_.extend(options,{data: this.getCriteria()}));
            return collection.fetch(_.extend(options,{data: this.getCriteria()}));
        };

        this.pagination = function (count) {
            if ( _.isNumber(count)) {
                _pagination.countIsChanged = count != _pagination.count_items;
                _pagination.count_items = count;
                if(_pagination.count_items / _pagination.per_page > 0){
                    _pagination.count_pages =   ((_pagination.count_items + _pagination.per_page -1) / _pagination.per_page) | 0;
                    if(_pagination.count_pages > 1){
                        if(_pagination.cur_page > _pagination.count_pages){
                            _pagination.cur_page = 1;
                        }
                        _pagination.next_page = _pagination.cur_page+1 <= _pagination.count_pages && _pagination.cur_page+1;
                        _pagination.prev_page = _pagination.cur_page-1 >= 1 && _pagination.cur_page-1;
                        _pagination.pages_items = [];
                        for(var i = 1; i <=  _pagination.count_pages; i++){
                            _pagination.pages_items.push(i);
                        }
                    }else{
                        _pagination.count_items = false;
                    }
                }else{
                    _pagination.count_items = false;
                }
                collection.trigger('pagination:reset')
            }
            return _pagination.count_items && _pagination;
        };

        this.page = function(page,per_page){
            page = page || 1;
            per_page = per_page || 10;
            _pagination.cur_page = +page;
            _pagination.per_page =  +per_page;
            _criteria.skip = (_pagination.cur_page-1)*_pagination.per_page;
            _criteria.limit = _pagination.per_page;
            return this;
        };

        this.nextPage = function(per_page){
            per_page = per_page || 10;
            if(this.pagination() && this.pagination().next_page){
                this.page(this.pagination().next_page, per_page );
            }
            return this;
        };

        this.limit = function(num){
            _criteria.limit = num;
            return this;
        };

        return this;
    };


   export default Backbone.Collection.extend({


        Find: function (clear) {
            if (_.isUndefined(this._server)) {
                this._server = new server(this);
            }
            !_.isUndefined(clear) && this._server.clearCriteria();
            return this._server;
        },

       /*afterFetch:function(fetch){
           return fetch.then(function(){
              return Promise.resolve(fetch)
           })
       },*/

        getModel:function(id){

        }
    });

