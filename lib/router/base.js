
var querystring = require('querystring');
var _ = require('underscore');
class baseRouter{

    constructor(config){
        this.config = config.Router;
        this.routes = this.prepareRoutes(this.config.routes);
        this.baseController  = config.Router.baseController;
        this.definedControllers = this.autoDefineController();
    }

    prepareRoutes(routes){
        return routes.map((route)=>{
            let singleParamReg = /(\/:(\w+))/g
            if(typeof route.url !== 'string' || typeof route.controller !== 'string' || typeof route.action !== 'string'){
                return;
            }
            route.singleParamKeys = route.url.match(singleParamReg);
            if(route.singleParamKeys){
                route.singleParamKeys = route.singleParamKeys.map(function(r){
                    return r.replace('/:','');
                })
            }
            route.urlReg = '^(\\/?' + route.url.replace(singleParamReg,'(?:\\/([\\w-:]+))?') + ')((?:\\/\\w+=[\\w-]+)+)*(?:\\/)?$';
            route.action = 'action' + route.action.charAt(0).toUpperCase() + route.action.slice(1);
            route.controller =   route.controller.charAt(0).toUpperCase() + route.controller.slice(1) + 'Controller';
            return route;
        });
    }

    trigger(url){
        try{
            let req = this.findRoute(url);
            let controller =  require('app/controllers/' + req.controller);
            controller = new controller(req);
            return controller.exec(req.action, [req.urlParams]);
        }catch(e){
            let controller = new this.baseController({url:url});
            return controller.exec();
        }
    }

    findRoute(url){
        let parsedData;
        if((parsedData = this.findInConfig(url))) {
            return parsedData;
        }
        throw new Error('Route Not Found');
    }

    findInConfig(url){
        let parsedData;
        for(var i = 0; i < this.routes.length; i++){
            if(parsedData = this.parseUrl(this.routes[i],url)){
                break;
            }
        }
        return parsedData;
    }

    parseUrl(route,url){
        let req,parsed;
        if(!(parsed = url.match(new RegExp(route.urlReg)))){
            return null;
        }
        req = _.extend({},route);
        req.originalUrl = url;
        parsed.shift();
        req.urlPath = parsed.shift();
        req.urlParamsString = parsed.pop();
        if(typeof  req.urlParamsString === 'string' && req.urlParamsString[0] == '/'){
            req.urlParamsString = req.urlParamsString.substr(1);
        }
        req.urlParams = _.extend(querystring.parse(req.urlParamsString,'/','='),_.object(route.singleParamKeys,parsed))
        return req;
    }

    findInDefinedControllers(url){

    }

    autoDefineController(){
        function requireAll(r) {
            r.keys().forEach(r);
            return r.keys().map(function(val){
                return val
                    .replace('Controller','')
                    .replace('./','')
                    .replace(/\.js(x)?$/,'');
            });
        }
        return requireAll(require.context('app/controllers', true, /\.js(x)?$/));
    }

    urlParamsToObject(url,routeRegExp){
        let urlParams = url.replace(routeRegExp,'');
        urlParams = decodeURIComponent(urlParams);
        var p = {};
        var data = urlParams.match(this.config.parseUrl.urlParamsDelimiter);
        if(data !== null){
            data.map((val)=>{
                let keyVal = val.split(this.config.parseUrl.urlParamKeyValueDelimiter);
                p[keyVal[0]] = keyVal[1];
            });
        }
        return p;
    }
}

export default  baseRouter;