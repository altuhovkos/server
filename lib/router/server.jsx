var express        = require('express');
var bodyParser     = require('body-parser');
var methodOverride = require('method-override');
var compress = require('compression')();
var BaseRouter = require('./base');
var _ = require('underscore');

class Router extends BaseRouter{
    constructor(config){
        super(config);
        var  app, staticFiles;
        this._app = app = express();
        app.use(compress);
        app.use(bodyParser.json());
        app.use(bodyParser.json({ type: 'application/vnd.api+json' }));
        app.use(bodyParser.urlencoded({ extended: true }));
        app.use(methodOverride('X-HTTP-Method-Override'));
        staticFiles = express.static(publicPath);
        app.get(/^\/(css|js|font|img|file|favicon\.ico|robots\.txt)+(.+)?/,function(req, res, next){
            staticFiles(req, res, next);
        });
        var router = this;
        app.use(function(req, res, next) {
            if (req.method.toLowerCase() !== 'get') {
                next();
                return;
            }
            let data = router.trigger(req.originalUrl);
            data.then(([html, statusCode, loc])=>{
                if (typeof statusCode === 'string') {
                    return res.redirect(301, statusCode);
                }
                res.status(statusCode);
                res.send(html);
            }).catch((err)=>{
                res.status(500).end();
            });
        });
    }



    start(){
        this._app.listen(process.env.PORT || 3000);
    }
}

module.exports = Router;

