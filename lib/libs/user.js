var _ = require('underscore');

export default class User {

    get defaultData() {
        return {
            id: null,
            token: null
        }
    }

    _data;

    constructor(...params) {
        if(CLIENT){
            this.clientInit(...params);
        }else{
            this.serverInit(...params);
        }
    }

    serverInit(token = false, id = false){
        this._data = this.defaultData;
    }

    clientInit(token = false, id = false){
        if(window._user != undefined){
            this._data = window._user;
        }else if(localStorage.getItem('userAuthToken')){
            this._data = window._user = {
                id : localStorage.getItem('userId') || null,
                token : localStorage.getItem('userAuthToken')
            };
        }else{
            this._data = window._user = this.defaultData;
        }

        if(token){
            this.token = token;
        }
        if(id){
            this.id = id;
        }
    }

    set token(token){
        this._data.token  = window._user.token = token;
        localStorage.setItem('userAuthToken',token)
    }

    get token(){
        return this._data.token;
    }

    get id(){
        return this._data.id;
    }

    set id(id){
        this._data.id = window._user.id = id;
        localStorage.setItem('userId',id)
    }

    get isAuthorized(){
        return !!this._data.token;
    }

    logout(){
        this._data = window._user = this.defaultData;
        localStorage.removeItem('userId');
        localStorage.removeItem('userAuthToken');
    }
}