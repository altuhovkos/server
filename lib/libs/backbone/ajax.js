(function() {
    'use strict';

    var defaults = function(obj, source) {
        for (var prop in source) {
            if (obj[prop] === undefined) obj[prop] = source[prop];
        }
        return obj;
    };

    var stringifyGETParams = function(url, data) {
        var query = '';
        for (var key in data) {
            if (data[key] == null) continue;
            query += '&'
                + encodeURIComponent(key) + '='
                + encodeURIComponent(data[key]);
        }
        if (query) url += (~url.indexOf('?') ? '&' : '?') + query.substring(1);
        return url;
    };

    var checkStatus = function(response) {
        if (response.status >= 200 && response.status < 300) {
            return response;
        } else {
            var error = new Error(response.statusText);
            error.statusCode = response.status;
            error.response = response;
            throw error;
        }
    };

    var headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    };

    var ajax = function(options) {
        if (options.type === 'GET' && typeof options.data === 'object') {
            options.url = stringifyGETParams(options.url, options.data);
            delete options.data;
        }

        return fetch(options.url, defaults(options, {
            method: options.type,
            headers: defaults(options.headers || {}, headers),
            body: options.data,
            timeout: 2000
        })).then(checkStatus).catch(function(error){
            throw error
        }).then(function(response) {
            return options.dataType === 'json' ? response.json(): response.text();
        });
    };

    ajax.setHeaders = function(obj){
        for (var prop in obj) {
            headers[prop] = obj[prop];
        }
    };

    ajax.unsetHeader = function(name){
        return delete headers[name];
    };

    if (typeof exports === 'object') {
        module.exports = ajax;
    } else {
        Backbone.ajax = ajax;
    }
})();