import Backbone from 'backbone';
var _ = require('underscore');
import ObjectCriteria from '../helpers/ObjectCriteria';
var querystring = require('querystring');
class navigation {

    constructor(req) {
        this.req = req;
    }

    createUrl(url, urlParams = {},urlParamsCriteria = false) {
        url = url || this.req.url;
        urlParams = _.clone(urlParams);
        let singleParamReg = /(\/:(\w+))/g,
            singleParamKeys = url.match(singleParamReg);
        if (singleParamKeys) {
            singleParamKeys = singleParamKeys.map(function (r) {
                return r.replace('/:', '');
            });
            for (var i = 0; i < singleParamKeys.length; i++) {
                let key = singleParamKeys[i];
                if (typeof urlParams[key] == 'string' || typeof urlParams[key] == 'number') {
                    url = url.replace(':' + key, urlParams[key]);
                    urlParams[key] = null;
                } else {
                    url = url.replace(':' + key, '');
                }
            }
        }
        if(this.req.urlPath == url && urlParamsCriteria){
            urlParams = ObjectCriteria(this.req.urlParams,urlParams)
        }
        for(var v in urlParams){
            if(urlParams[v] == null){
                delete urlParams[v];
            }
        }
        let strUrlParams = querystring.stringify(urlParams, '/', '=');
        if (strUrlParams != '') {
            (url[url.length - 1] != '/') && (url += '/');
            url += strUrlParams;
        }
        return url
    }

    get url() {
        return this.req.urlPath;
    }
    to(url, urlParams, urlParamsCriteria) {
        if(CLIENT){
            url = this.createUrl(url, urlParams, urlParamsCriteria);
            Backbone.history.navigate(url,{trigger: true});
            if (url == this.req.originalUrl) {
                Backbone.history.loadUrl(url, {trigger: true});
            }
        }
    }

    reload(urlParams, urlParamsCriteria) {
        this.to(this.createUrl(null, urlParams, urlParamsCriteria));
    }
}

export default  navigation;