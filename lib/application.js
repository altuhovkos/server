require("babel-polyfill");
require('isomorphic-fetch');
var _ = require('underscore');
require('./libs/backbone');
var merge = require('deepmerge');
var defaultConfig = require('./config');

class App{

    constructor(config = {}){
        this._config = merge(defaultConfig,config);
        var Router = this._config.Router.instance();
        this.router =  new Router(this._config);
        this.start();
    }
    start(){
             this.router.start();
    }
}

module.exports = App;

